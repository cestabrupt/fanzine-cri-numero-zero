# ~/ABRÜPT/FANZINE CRI/NUMÉRO ZÉRO/*

La [page de ce livre](https://abrupt.ch/fanzine-cri/numero-zero/) sur le réseau.

## Sur le livre

FANZINE CRI NUMÉRO ZÉRO :

* Sacs d’os par Évariste Corbeau
* Mister & Miss van der Farmworst par Edith Urfilz
* Hacker ouvert par Lumpen+Clebs
* Polèmes par Cécile Toussaint
* De crasse en silicium par Ossip Tikhonov
* Dubio par Dorno
* Adverretiser par El Daurade
* Histoires barbantes par Nicht Nacht
* Cuckoo’s space par Monsieur Dope
* Zeptofictions par Ann Persson
* Dr. Punkt par Don A.I.
* Espace chacal par Beata Raoul
* Archiboom par Archidoom

## Sur le Fanzine

Le Fanzine Cri se fixe en intraveineuse sur notre irréalité. Il est le chiffre ou l’évasion.

## Sur la licence

Cet [antifanzine](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d’informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
